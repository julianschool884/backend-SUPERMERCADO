import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usuarios } from './entities/Usuarios.entity';
import { EdicionEncargadoController } from './SuperAdministrador/edicionEncargado/edicionEncargado.controller';
import { EdcionEncargadoService } from './SuperAdministrador/edicionEncargado/edicionEncargado.service';
import { RegistroSupermercadoController } from './Encargado/registroSupermercado/registroSupermercado.controller';
import { RegistroSupermercadoService } from './Encargado/registroSupermercado/registroSupermercado.service';
import { RegistroSupermercado } from './entities/registroSupermercado.entity';
import { ListadoSupermercadoController } from './SuperAdministrador/listadoSupermercado/listadoSupermercado.controller';
import { ListadoSupermercadoService } from './SuperAdministrador/listadoSupermercado/listadoSupermercado.service';
import { RegistroDepartamento } from './entities/Departamento.entity';
import { RegistroDepartamentosService } from './Encargado/Departamentos/Departamentos.service';
import { RegistroDepartamentosController } from './Encargado/Departamentos/Departamentos.controller';
import { Trabajadores } from './entities/Trabajadores.entity';
import { TrabajadoresController } from './Encargado/Trabajadores/Trabajadores.controller';
import { TrabajadoresService } from './Encargado/Trabajadores/Trabajadores.service';
import { loginController } from './login/login.controller';
import { loginService } from './login/login.service';
import { ListadoEncargadoController } from './SuperAdministrador/listadoEncargado/listadoEncargado.controller';
import { ListadoEncargadoService } from './SuperAdministrador/listadoEncargado/listadoEncargado.service';
import { RegitroService } from './Usuarios/Usuarios.service';
import { RegitroController } from './Usuarios/Usuarios.controller';


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'mercado',
      password: '12345',
      database: 'supermercado',
      entities: ["dist/**/*.entity{.ts,.js}"],
      synchronize: true,
    }),
    TypeOrmModule.forFeature(
      [
       Usuarios,
       RegistroSupermercado,
       RegistroDepartamento,
       Trabajadores,
       ]),
  ],

  controllers: [
    AppController,
    EdicionEncargadoController,
    RegistroSupermercadoController,
    ListadoSupermercadoController,
    RegistroDepartamentosController,
    TrabajadoresController,
    ListadoEncargadoController,
    loginController,
    RegitroController,
  ],
  
  providers: [
    AppService,
    EdcionEncargadoService, 
    RegistroSupermercadoService,
    ListadoSupermercadoService,
    RegistroDepartamentosService,
    TrabajadoresService,
    ListadoEncargadoService,
    loginService,
    RegitroService,
  ],
})
export class AppModule {}
