export class RegistroSupermercadoDto {
    readonly idencargado: number;
    readonly nombreSupermercado: string;
    readonly calle: string;
    readonly numero: string;
    readonly codigoPostal: string;
    readonly coloniaDelegacion: string;
    readonly estado: string;
    readonly ciudad: string;
    readonly razonSocial: string;
    readonly correo: string;
    readonly telefono: string;
}