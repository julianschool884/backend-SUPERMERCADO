import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegistroSupermercado } from '../../entities/registroSupermercado.entity';

//Listado
@Injectable()
export class ListadoSupermercadoService {

    constructor(
    @InjectRepository(RegistroSupermercado)
    private readonly listadoSupermercadoRepository: Repository<RegistroSupermercado>,){}

    //listado de encargados
    async listadoSupermercado(){
        return await this.listadoSupermercadoRepository.find();
    }     
}