import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RegistroSupermercadoDto } from  '../../dto/registroSupermercado';
import { RegistroSupermercado } from '../../entities/registroSupermercado.entity';
import { Usuarios } from '../../entities/Usuarios.entity';


//SuperMercado
@Injectable()
export class RegistroSupermercadoService {

    constructor(
    @InjectRepository(RegistroSupermercado)
    private readonly registroSupermercadoRepository: Repository<RegistroSupermercado>,
    @InjectRepository(Usuarios)
    private readonly usuariosRepository: Repository<Usuarios>,
    ){}

    //listado supermercado
    async getAll() {
        return await this.registroSupermercadoRepository.find();
    }

    //registrar supermercado
    async Register( registroNuevo: RegistroSupermercadoDto): Promise<RegistroSupermercado>{
        const nuevo = new RegistroSupermercado();
        const usuarioid = await this.usuariosRepository.findOne(registroNuevo.idencargado);

        nuevo.id
        nuevo.encargado = usuarioid
        nuevo.nombreSupermercado = registroNuevo.nombreSupermercado;
        nuevo.calle = registroNuevo.calle;
        nuevo.numero = registroNuevo.numero;
        nuevo.codigoPostal = registroNuevo.codigoPostal;
        nuevo.coloniaDelegacion = registroNuevo.coloniaDelegacion;
        nuevo.estado = registroNuevo.estado;
        nuevo.ciudad = registroNuevo.ciudad;
        nuevo.razonSocial = registroNuevo.razonSocial;
        nuevo.correo = registroNuevo.correo;
        nuevo.telefono = registroNuevo.telefono;

        return this.registroSupermercadoRepository.save(nuevo);
    }
}
