import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TrabajadoresDto } from  '../../dto/Trabajadores'
import { Trabajadores } from '../../entities/Trabajadores.entity';
import { RegistroDepartamento} from '../../entities/Departamento.entity'

//Trabajadores
@Injectable()
export class TrabajadoresService {

    constructor(
    @InjectRepository(Trabajadores)
    private readonly trabajadoresRepository: Repository<Trabajadores>,
    @InjectRepository(RegistroDepartamento)
    private readonly departamentosRepository: Repository<RegistroDepartamento>,){}

    //listado Trabajadores
    async getAll() {
        return await this.trabajadoresRepository.find();
    }

    //registrar Trabajador
    async Register(trabajadoresNuevo: TrabajadoresDto): Promise<Trabajadores>{
        const nuevo = new Trabajadores();
        const departamentoid = await this.departamentosRepository.findOne(trabajadoresNuevo.departamentoid);

        nuevo.departamento = departamentoid
        nuevo.IDTrabajador = trabajadoresNuevo.IDTrabajador;
        nuevo.nombre = trabajadoresNuevo.nombre;
        nuevo.apellidos = trabajadoresNuevo.apellidos;
        nuevo.diasLaborales = trabajadoresNuevo.diasLaborales;
        nuevo.telefono = trabajadoresNuevo.telefono;
        
        return this.trabajadoresRepository.save(nuevo);
    }

    //listado departamentos:::Parte del registro va los "Departamentos"
    async getAllDepartamentos() {
        return await this.departamentosRepository.find();
    }


    //Eliminar trabajador
    async deleteDepartamento(id: number): Promise<any>{
        return await this.trabajadoresRepository.delete(id);
    }

    //Edicion trabajador
    async updateTrabajador(idEncargado:number, TrabajadoroActulizar: TrabajadoresDto): Promise<Trabajadores>{
        const encargadoUpdate = await this.trabajadoresRepository.findOne(idEncargado);
        
        encargadoUpdate.IDTrabajador = TrabajadoroActulizar.IDTrabajador;
        encargadoUpdate.nombre = TrabajadoroActulizar.nombre;
        encargadoUpdate.apellidos = TrabajadoroActulizar.apellidos;
        encargadoUpdate.diasLaborales = TrabajadoroActulizar.diasLaborales;
        encargadoUpdate.telefono = TrabajadoroActulizar.telefono;

        return await this.trabajadoresRepository.save(encargadoUpdate)
    }
}
