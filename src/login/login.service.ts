import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Usuarios } from '../entities/Usuarios.entity'

//Login 
@Injectable()
export class loginService {

    constructor(
    @InjectRepository(Usuarios)
    private readonly loginRepository: Repository<Usuarios>,){}

    //Login Encargado
    async Login( iniciarsesion: Usuarios ){
        const user = await this.loginRepository.findOne(iniciarsesion);
        const payload = { correo: user.correo, contrasena: user.contrasena, id: user.id , tipoUsuario: user.tipoUsuario};
        return (payload)
    }  
}
