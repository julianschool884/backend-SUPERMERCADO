import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsuariosDto } from '..//dto/Usuarios';
import { Usuarios } from '../entities/Usuarios.entity';

//Registro 
@Injectable()
export class RegitroService {

    constructor(
    @InjectRepository(Usuarios)
    private readonly regitroEncargadoRepository: Repository<Usuarios>,){}

    //listado de encargados
    async getAll() {
        return await this.regitroEncargadoRepository.find();
    }

    //registrar encargado
    async Register(registroNuevo: UsuariosDto): Promise<Usuarios>{
        const nuevo = new Usuarios();

        nuevo.nombre = registroNuevo.nombre;
        nuevo.apellidoP = registroNuevo.apellidoP;
        nuevo.apellidoM = registroNuevo.apellidoM;
        nuevo.fecha_Nacimiento = registroNuevo.fecha_Nacimiento;
        nuevo.genero = registroNuevo.genero;
        nuevo.correo = registroNuevo.correo;
        nuevo.telefono = registroNuevo.telefono;
        nuevo.contrasena = registroNuevo.contrasena;
        nuevo.Confirmacion_contrasena = registroNuevo.Confirmacion_contrasena;

        return this.regitroEncargadoRepository.save(nuevo);
    }
}