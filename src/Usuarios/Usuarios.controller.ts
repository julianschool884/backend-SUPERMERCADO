import { Controller,Post,Get,Put,Res,Body,HttpStatus,Param } from '@nestjs/common';
import { UsuariosDto } from '../dto/Usuarios';
import { RegitroService } from './Usuarios.service'

//Registro 
@Controller('Registrar')
export class RegitroController {
    constructor(private regitroService: RegitroService){
    }

    //registro encargados
    @Post()
    create (@Body() registerDo: UsuariosDto, @Res() response){
        this.regitroService.Register(registerDo).then( registro => {
            response.status(HttpStatus.CREATED).json(registro);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({mensaje: "error en tu registro"});});
    }

    //lista de usuarios
    @Get()
    getAll(@Res() response){
        this.regitroService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de registro'});
        })
    }
}