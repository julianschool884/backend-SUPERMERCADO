import { Controller,Get,Res,HttpStatus } from '@nestjs/common';
import { ListadoEncargadoService } from './listadoEncargado.service'

//Registro
@Controller('ListadoEncargados')
export class ListadoEncargadoController {
    constructor(private listadoEncargadoService: ListadoEncargadoService){
    }

    //lista de encargado
    @Get()
    getAll(@Res() response){
        this.listadoEncargadoService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion de registro'});
        })
    }
}