import { Controller,Put,Res,Body,HttpStatus,Param } from '@nestjs/common';
import {UsuariosDto} from '../../dto/Usuarios';
import { EdcionEncargadoService } from './edicionEncargado.service';

//Ediccion
@Controller('EdicionEncargados')
export class EdicionEncargadoController {
    constructor(private edicionEncargadoService: EdcionEncargadoService){};

    //edicion de encargado 
    @Put(':id')
    update(@Body() updateEncargadoDo: UsuariosDto, @Res() response, @Param('id') idEncargado){
        this.edicionEncargadoService.updateEncargado(idEncargado, updateEncargadoDo). then(Encargado => {
            response.status(HttpStatus.OK).json(Encargado);
        }).catch (() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la edicion del encargado'});
        });
    } 
}