import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { RegistroSupermercado } from '../entities/registroSupermercado.entity';

@Entity()
    export class Usuarios {
        @PrimaryGeneratedColumn()
        id: number;

        @Column()
        nombre: string;

        @Column({default: "ENCARGADO"})
        tipoUsuario: string;

        @Column()
        apellidoP: string;

        @Column()
        apellidoM: string;
        
        @Column()
        fecha_Nacimiento: string;

        @Column()
        genero: string;

        @Column({unique: true})
        correo: string;

        @Column({unique: true})
        telefono: string;

        @Column()
        contrasena: string;

        @Column()
        Confirmacion_contrasena: string;


}
