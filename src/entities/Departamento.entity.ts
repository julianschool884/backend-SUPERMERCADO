import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { RegistroSupermercado } from '../entities/registroSupermercado.entity';


@Entity()
    export class RegistroDepartamento {
        @PrimaryGeneratedColumn()
        id: number;

        @Column({unique: true})
        nombreDepartamento: string;

        //Relaciones
        @ManyToOne(() => RegistroSupermercado, RegistroSupermercado => RegistroSupermercado.nombreSupermercado)
        supermercado: RegistroSupermercado;         
    }