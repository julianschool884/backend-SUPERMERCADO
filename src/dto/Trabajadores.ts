export class TrabajadoresDto {
    readonly departamentoid: number;
    readonly IDTrabajador: string;
    readonly nombre: string;
    readonly apellidos: string;
    readonly diasLaborales: string;
    readonly telefono: string;
}