import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Usuarios } from '../entities/Usuarios.entity';

@Entity()
    export class RegistroSupermercado {
        @PrimaryGeneratedColumn()
        id: number;

        @Column({unique: true})
        nombreSupermercado: string;

        @Column()
        calle: string;

        @Column()
        numero: string;
        
        @Column()
        codigoPostal: string;

        @Column()
        coloniaDelegacion: string;

        @Column()
        estado: string;

        @Column()
        ciudad: string;

        @Column({unique: true})
        razonSocial: string;

        @Column({unique: true})
        correo: string;

        @Column({unique: true})
        telefono: string;

        //Relaciones   
        @ManyToOne(() => Usuarios, usuarios => usuarios.id)
        encargado: Usuarios;

        
}
