import { Post, Controller, Body, Res, HttpStatus } from '@nestjs/common';
import { loginService } from './login.service';
import { Usuarios } from '../entities/Usuarios.entity'

//Login
@Controller()
export class loginController {
    constructor(private readonly loginService: loginService){}

    //Login de los usuarios
    @Post('Login')
    async Login(@Body() loginDo : Usuarios,@Res() response){
        this.loginService.Login(loginDo).then( login => {
            response.status(HttpStatus.OK).json(login);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({mensaje: "Correo electrónico y/o contraseña no válidos. Por favor, inténtalo de nuevo"});
      });
   }
}