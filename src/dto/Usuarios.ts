export class UsuariosDto {
    readonly nombre: string;
    readonly apellidoP: string;
    readonly apellidoM: string;
    readonly fecha_Nacimiento: string;
    readonly genero: string;
    readonly correo: string;
    readonly telefono: string;
    readonly contrasena: string;
    readonly Confirmacion_contrasena: string;
}