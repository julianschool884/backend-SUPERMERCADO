import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { RegistroDepartamento } from '../entities/Departamento.entity';

@Entity()
    export class Trabajadores{
        @PrimaryGeneratedColumn()
        id: number;

        @Column({unique: true})
        IDTrabajador: string;

        @Column()
        nombre: string;

        @Column()
        apellidos: string;

        @Column()
        diasLaborales: string;
        
        @Column({unique: true})
        telefono: string;

        @ManyToOne(() => RegistroDepartamento, registroDepartamento => registroDepartamento.nombreDepartamento)
        departamento: RegistroDepartamento;

}