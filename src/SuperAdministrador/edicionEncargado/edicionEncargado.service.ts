import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UsuariosDto } from '../../dto/Usuarios';
import { Usuarios } from '../../entities/Usuarios.entity';

@Injectable()
export class EdcionEncargadoService {

    constructor(
    @InjectRepository(Usuarios)
    private readonly edicionEncargadoRepository: Repository<Usuarios>,){}

    //edicion de encargado
    async updateEncargado(idEncargado:number, EncargadoActulizar: UsuariosDto): Promise<Usuarios>{
        const encargadoUpdate = await this.edicionEncargadoRepository.findOne(idEncargado);
        encargadoUpdate.nombre = EncargadoActulizar.nombre;
        encargadoUpdate.apellidoP = EncargadoActulizar.apellidoP;
        encargadoUpdate.apellidoM = EncargadoActulizar.apellidoM;
        encargadoUpdate.fecha_Nacimiento = EncargadoActulizar.fecha_Nacimiento;
        encargadoUpdate.genero = EncargadoActulizar.genero;
        encargadoUpdate.correo = EncargadoActulizar.correo;
        encargadoUpdate.telefono = EncargadoActulizar.telefono;
        encargadoUpdate.contrasena = EncargadoActulizar.contrasena;
        encargadoUpdate.Confirmacion_contrasena = EncargadoActulizar.Confirmacion_contrasena;

        return await this.edicionEncargadoRepository.save(encargadoUpdate)
    }

}
