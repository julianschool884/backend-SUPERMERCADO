import { Controller,Get,Res,HttpStatus} from '@nestjs/common';
import { ListadoSupermercadoService} from '../listadoSupermercado/listadoSupermercado.service';

//listado
@Controller('ListadoSupermercado')
export class ListadoSupermercadoController {
    constructor(private listadoSupermercadoService: ListadoSupermercadoService){
    }

    //lista de supermercado
    @Get()
    async getAll(@Res() response ){
        this.listadoSupermercadoService.listadoSupermercado().then( listadoSupermercado => {
            response.status(HttpStatus.OK).json(listadoSupermercado);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json ({mensaje: "error en la obtencion listado de supermercado'"});
      });
   }
}
