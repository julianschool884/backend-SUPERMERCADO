import { Controller,Post,Get,Res,Body,HttpStatus} from '@nestjs/common';
import { RegistroSupermercadoDto } from '../../dto/registroSupermercado';
import { RegistroSupermercadoService } from '../registroSupermercado/registroSupermercado.service';

//SuperMercado
@Controller('RegistroSupermercado')
export class RegistroSupermercadoController {
    constructor(
        private superAdminService: RegistroSupermercadoService){
    }

    //crear supermercado
    @Post()
    create (@Body() registerDo: RegistroSupermercadoDto,@Res() response){
        this.superAdminService.Register(registerDo).then( registro => {
            response.status(HttpStatus.CREATED).json(registro);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({mensaje: "error en tu registro"});});
    }

    //lista del supermercado 
    @Get()
    getAll(@Res() response){
        this.superAdminService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion listado de supermercado'});
        })
    }
}