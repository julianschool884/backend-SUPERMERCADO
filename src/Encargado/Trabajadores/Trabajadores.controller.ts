import { Controller,Post,Get,Res,Body,HttpStatus, Delete, Param, Put } from '@nestjs/common';
import { TrabajadoresDto } from '../../dto/Trabajadores';
import { TrabajadoresService } from './Trabajadores.service'
import { RegistroDepartamentosService } from '../Departamentos/Departamentos.service'

//Trabajadores
@Controller()
export class TrabajadoresController {
    constructor(
        private trabajadoresService: TrabajadoresService,
        private registroDepartamentosAdminService: RegistroDepartamentosService,
    ){}

    //crear  trabajador
    @Post('RegistroTrabajadores')
    create (@Body() registerDo: TrabajadoresDto, @Res() response){
        this.trabajadoresService.Register(registerDo).then( registro => {
            response.status(HttpStatus.CREATED).json(registro);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({mensaje: "error en tu registro"});});
    }

    //Parte del registro va los ""Departamentos"
    @Get('ListadoDepartamentos')
    getAllDepartamentos(@Res() response){
        this.registroDepartamentosAdminService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion listado departamentos'});
        })
    }

    //listado de trabajadores
    @Get('ListadoTrabajadores')
    getAll(@Res() response){
        this.trabajadoresService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion listado trabajadores'});
        })
    }

    //eliminar trabajador
    @Delete('EliminarTrabajador/:id')
    Delete(@Res() response, @Param('id') idDepartamento){
        this.trabajadoresService.deleteDepartamento(idDepartamento).then(res => {
            response.status(HttpStatus.OK).json(res);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la eliminacion del trabajador'});
        })
    }

    //edicion trabjador
    @Put('ActulizarTrabajador/:id')
    update(@Body() updateEncargadoDo: TrabajadoresDto, @Res() response, @Param('id') idEncargado){
        this.trabajadoresService.updateTrabajador(idEncargado, updateEncargadoDo). then(Encargado => {
            response.status(HttpStatus.OK).json(Encargado);
        }).catch (() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la edicion del encargado'});
        });
    } 
}