import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { registroDepartamentoDto } from  '../../dto/Departamento';
import { RegistroDepartamento } from '../../entities/Departamento.entity';
import { RegistroSupermercado } from '../../entities/registroSupermercado.entity';

//Departamentos
@Injectable()
export class RegistroDepartamentosService {

    constructor(
    @InjectRepository(RegistroDepartamento)
    private readonly departamentosRepository: Repository<RegistroDepartamento>,
    @InjectRepository(RegistroSupermercado)
    private readonly registroSupermercado: Repository<RegistroSupermercado>,
    ){}

    //listado departamentos
    async getAll() {
        return await this.departamentosRepository.find();
    }

    //registrar departamento
    async Register(registroNuevo: registroDepartamentoDto): Promise<RegistroDepartamento>{
        const nuevo = new RegistroDepartamento();
        const supermerid = await this.registroSupermercado.findOne(registroNuevo.idsupermercado);

        nuevo.supermercado = supermerid
        nuevo.nombreDepartamento = registroNuevo.nombreDepartamento;
        
        return this.departamentosRepository.save(nuevo);
    }

    //Eliminar departamento
    async deleteDepartamento(id: number): Promise<any>{
        return await this.departamentosRepository.delete(id);
    }
}
