
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { RegistroSupermercado } from 'src/entities/registroSupermercado.entity';
import { Repository } from 'typeorm';
import { Usuarios } from '../../entities/Usuarios.entity';

//Registro 
@Injectable()
export class ListadoEncargadoService {

    constructor(
    @InjectRepository(RegistroSupermercado)
    private readonly listadoSupermercadoRepository: Repository<RegistroSupermercado>,
    @InjectRepository(Usuarios)
    private readonly listadoEncargadoRepository: Repository<Usuarios>,
    ){}

    //listado de encargados
    async getAll() {
        const lista = new Usuarios();
        const Respuesta= [];
        if(lista ){
            lista.tipoUsuario = "ENCARGADO";
        }
        const encargadosArr = await this.listadoEncargadoRepository.find(lista);

        console.log(encargadosArr);
        
        for (const encargados of encargadosArr) {
            const supermercado = await this.listadoSupermercadoRepository.findOne({where:{encargado:encargados.id}});
            if (supermercado) {
                const nombreSuper = supermercado.nombreSupermercado;
                const aux = {...encargados, nombreSuper}
                Respuesta.push(aux);
                
            }else{
                Respuesta.push(encargados);
            }
        }
        return Respuesta;
    }
}