import { Controller,Post,Get,Res,Body,HttpStatus, Delete, Param } from '@nestjs/common';
import { registroDepartamentoDto } from '../../dto/Departamento';
import { RegistroDepartamentosService } from './Departamentos.service'

//Departamentos
@Controller()
export class RegistroDepartamentosController {
    constructor(private registroDepartamentosAdminService: RegistroDepartamentosService){}

    //crear  departamennro
    @Post('RegistroDepartamento')
    create (@Body() registerDo: registroDepartamentoDto, @Res() response){
        this.registroDepartamentosAdminService.Register(registerDo).then( registro => {
            response.status(HttpStatus.CREATED).json(registro);
        }).catch( () => {response.status(HttpStatus.FORBIDDEN).json ({mensaje: "error en tu registro"});});
    }

    //listado del departamento
    @Get('ListadoDepartamentos')
    getAll(@Res() response){
        this.registroDepartamentosAdminService.getAll().then(registrosList => {
            response.status(HttpStatus.OK).json(registrosList);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la obtencion listado departamentos'});
        })
    }

    //eliminar departamento 
    @Delete('EliminarDepartamento/:id')
    Delete(@Res() response, @Param('id') idDepartamento){
        this.registroDepartamentosAdminService.deleteDepartamento(idDepartamento).then(res => {
            response.status(HttpStatus.OK).json(res);
        }).catch(() => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'error en la eliminacion del departamento'});
        })
    }
}